local inMobi = require( "plugin.inMobi" )

local placementID = "1474500368351"

local function adListener( event )
    if ( event.phase == "init" ) then
        inMobi.load( "banner", placementID )
    elseif ( event.phase == "loaded" ) then
        inMobi.show( event.type, event.placementId, { yAlign="bottom" } )
    elseif ( event.phase == "failed" ) then
        print( event.type )
        print( event.placementId )
        print( event.isError )
        print( event.response )
    end
end

inMobi.init( adListener, { accountId="c445e108f988444eb0a878f54d0d331f" } )