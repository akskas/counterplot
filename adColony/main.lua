     --
     -- AdColony
     --
     local adcolony = require( "plugin.adcolony" )

     -- AdColony listener function
     local function acListener( event )
      if ( event.phase == "init" ) then  -- Successful initialization
        print( event.provider )
      end
     end

     -- Initialize the AdColony plugin with your Corona Ads API key
     adcolony.init( acListener, { apiKey="0f1beeb2-d323-40f1-9caf-c6c6d5f9d8dc" } )

     -- Sometime later, show an ad
     if ( adcolony.isLoaded( "interstitial" ) ) then
       adcolony.show( "interstitial" )
     end