play_again_button = {}

local function handle_play_again()
    winner.hide()
    play_again_button.hide()

    rules_button.show()
    grid_lines.show()
    balls.show()
end

function play_again_button.show()
    if play_again_button_display.is_hidden == false then
        return true
    end

    play_again_button_display.button = widget.newButton({
        x = WIDTH/2,
        y = 0.8*HEIGHT,
        id = "play_again_button",
        label = "PLAY AGAIN",
        fontSize = 45,
        width = 400,
        height = 100,
        shape = "roundedRect",
        cornerRadius = 15,
        fillColor = { default={0.75,0.87,11,1}, over={0.56,0.78,1,1} },
        onPress = handle_play_again
    })

    play_again_button_display.is_hidden = false
    return true
end

function play_again_button.hide()
    if play_again_button_display.is_hidden == true then
        return true
    end

    if play_again_button_display.button ~= nil then
        play_again_button_display.button:removeSelf()
    end

    play_again_button_display = {
        button = {},
        is_hidden = true
    }

end