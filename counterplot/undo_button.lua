undo_button = {}

local function handle_undo()
    -- show banner ads
    ADS_TYPE = "banner"
    show_ads.show()

    balls.show_undo()
end

function undo_button.show()
    if undo_button_display.is_hidden == false then
        return true
    end

    undo_button_display.button = widget.newButton({
        left = WIDTH-250,
        top = 150,
        id = "undo_button",
        label = "UNDO",
        fontSize = 45,
        width = 200,
        height = 100,
        shape = "roundedRect",
        cornerRadius = 15,
        fillColor = { default={0.75,0.87,11,1}, over={0.56,0.78,1,1} },
        onPress = handle_undo
    })

    undo_button_display.is_hidden = false
    return true
end

function undo_button.hide()
    if undo_button_display.is_hidden == true then
        return true
    end

    if undo_button_display.button ~= nil then
        undo_button_display.button:removeSelf()
    end

    undo_button_display = {
        button = {},
        is_hidden = true
    }
end