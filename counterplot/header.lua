local header = {}

-- global functions
function modulus (num)
    if num < 0 then
        return -1*num
    else
        return num
    end
end

function change_current_player (player)
    if player == 'A' then
        matrix.current_player:setFillColor( 0.25, 1, 0.25, 1 )
    else
        matrix.current_player:setFillColor( 1, 0.25, 0.25, 1 )
    end
end

function organize_balls (event)
    for i = -2, 2 do
        for j = -3, 3 do
            if matrix[i][j] ~= nil then
                if matrix[i][j].ball ~= nil then
                    matrix[i][j].ball.x = X_ORIGIN + i*SIZE
                    matrix[i][j].ball.y = Y_ORIGIN + j*SIZE 
                    
                    if event == "ended" or event == "cancelled" then
                        if matrix[i][j].ball.markX ~= nil or matrix[i][j].ball.markY ~= nil then
                            matrix[i][j].ball.markX = nil
                            matrix[i][j].ball.markY = nil
                        end
                    end
                end
            end
        end
    end
end

function header.show()
    display.setDefault("background", 0, 0, 0)
    
    local appName = display.newText("COUNTERPLOT", 0, 100)
    appName:setFillColor(1, 1, 1)
    appName.x = display.contentCenterX

    -- require functions
    require('_touch_listener')
    require('balls')
    require('demo')
    require('demo_button')
    require('grid_lines')
    require('play_again_button')
    require('play_button')
    require('rules')
    require('rules_button')
    require('show_ads')
    require('undo_button')
    require('winner')
end

return header