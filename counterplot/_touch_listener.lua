local function _____show_winner()
    rules_button.hide()
    undo_button.hide()
    grid_lines.hide()
    balls.hide()

    winner.show()
end

local function  ____check_winner ()
    local a_count = 0
    local b_count = 0
    local winner_player = 'nil'
    
    for x = -2, 2 do
        for y = -3, 3 do
            if (matrix[x][y] ~= nil) and matrix[x][y].player ~= nil  then
                if matrix[x][y].player == 'A' then
                    a_count = a_count + 1 
                elseif matrix[x][y].player == 'B' then
                    b_count = b_count + 1
                end
            end
        end
    end

    if a_count == 0 and b_count ~= 0 then
        winner_player = 'B'
    elseif a_count ~= 0 and b_count == 0 then
        winner_player = 'A'
    end

    return winner_player
end

local function ____validate_diag_move (xStart, yStart, xEnd, yEnd)
    local isValid = false

    if (xStart + yStart) == 2 and (xEnd + yEnd) == 2 then
        isValid = true
    elseif (xStart - yStart) == 2 and (xEnd - yEnd) == 2 then
        isValid = true
    elseif (xStart + yStart) == -2 and (xEnd + yEnd) == -2 then
        isValid = true
    elseif (xStart - yStart) == -2 and (xEnd - yEnd) == -2 then
        isValid = true
    end

    return isValid
end

local function ___execute_end_move(xStart, yStart, xEnd, yEnd)
    local delX = modulus(xEnd - xStart)
    local delY = modulus(yEnd - yStart)
    local delXY = delX + delY

    local middleX = (xEnd + xStart) / 2
    local middleY = (yEnd + yStart) / 2

    local isValidMove = false
    local isValidDiagMove = false

    if matrix[xEnd][yEnd] ~= nil then
        if matrix[xEnd][yEnd].player == 'nil' then
            if delXY == 1 and matrix[xStart][yStart].player == WHOS_TURN then
                isValidMove = true
                LAST_MOVE_DIFF = 1
            end

            if delXY == 2 then
                if delX == 1 and delY == 1 and matrix[xStart][yStart].player == WHOS_TURN then
                    isValidDiagMove = ____validate_diag_move(xStart, yStart, xEnd, yEnd)
                    if isValidDiagMove == true then
                        isValidMove = true
                        LAST_MOVE_DIFF = 1
                    end
                else
                    if ((middleX % 1) == 0 and (middleY % 1) == 0) and matrix[middleX][middleY] ~= nil then
                        if matrix[middleX][middleY].player ~= nil and matrix[middleX][middleY].player ~= WHOS_TURN then
                            isValidMove = true
                            LAST_MOVE_DIFF = 2
                        end
                    end
                end
            end

            if delXY == 4 then
                if delX == 2 and delY == 2 then
                    if ((middleX % 1) == 0 and (middleY % 1) == 0) and matrix[middleX][middleY] ~= nil then
                        if matrix[middleX][middleY].player ~= nil and matrix[middleX][middleY].player ~= WHOS_TURN then
                            isValidDiagMove = ____validate_diag_move(xStart, yStart, xEnd, yEnd)
                            if isValidDiagMove == true then
                                isValidMove = true
                                LAST_MOVE_DIFF = 2
                            end
                        end
                    end
                end
            end
        end
    end

    -- if valid then execute end move
    if isValidMove == true then
        local ball = matrix[xStart][yStart].ball
        ball.x = X_ORIGIN + xEnd*SIZE
        ball.y = Y_ORIGIN + yEnd*SIZE

        matrix[xEnd][yEnd].ball = ball
        matrix[xEnd][yEnd].player = matrix[xStart][yStart].player
        matrix[xStart][yStart].ball = nil
        matrix[xStart][yStart].player = 'nil'

        if delXY == 4 or (delX == 2 or delY == 2) then
            -- remove middle ball and nil player
            matrix[middleX][middleY].ball:removeEventListener("touch", _touch_listener)
            matrix[middleX][middleY].ball:removeSelf()
            matrix[middleX][middleY].ball = nil
            matrix[middleX][middleY].player = 'nil'
        end

        if IS_UNDONE == true then
            IS_UNDONE = false
        end
        undo_button.show()

        LAST_X_START = xStart
        LAST_Y_START = yStart
        LAST_X_END = xEnd
        LAST_Y_END = yEnd
        LAST_PLAYER = WHOS_TURN


        -- change player turn
        if WHOS_TURN == 'A' then
            WHOS_TURN = 'B'
        else
            WHOS_TURN = 'A'
        end
        change_current_player(WHOS_TURN)

        -- invert touch listeners
        for x = -2, 2 do
            for y = -3, 3 do
                if (matrix[x][y] ~= nil) and matrix[x][y].ball ~= nil  then
                    if matrix[x][y].player ~= WHOS_TURN then
                        matrix[x][y].ball:removeEventListener("touch", _touch_listener)
                    else
                        matrix[x][y].ball:addEventListener("touch", _touch_listener)
                    end
                end
            end
        end

        local winner_player = ____check_winner()
        if winner_player ~= 'nil' then
            print(winner_player..' won!!!')
            _____show_winner()
        end
    end

    print('\n')
    print('xStart: '..xStart..' xEnd: '..xEnd)
    print('yStart: '..yStart..' yEnd: '..yEnd)
    print('middleX: '..middleX..'middleY: '..middleY)
    print('delX: '..delX..' delY: '..delY..' delXY: '..delXY)
    print('isValidMove: ')
    print(isValidMove)
    print('turn::::::::::::::: '..WHOS_TURN)

    return isValidMove
end

local function __end_touch_handler(xStartPix, yStartPix, xEndPix, yEndPix)
    -- check if ball's center is in one of the circles
    local xEndPixRem = modulus((xEndPix - xStartPix)) % SIZE
    local yEndPixRem = modulus((yEndPix - yStartPix)) % SIZE
    local delX = xEndPix - xStartPix
    local delY = yEndPix - yStartPix

    if (xEndPixRem < TOLERANCE) then
        if delX >= 0 then
            xEndPix = xEndPix - xEndPixRem
        else
            xEndPix = xEndPix + xEndPixRem
        end
    elseif xEndPixRem > (SIZE - TOLERANCE) then
        if delX >= 0 then
            xEndPix = xEndPix + (SIZE - xEndPixRem)
        else
            xEndPix = xEndPix - (SIZE - xEndPixRem)
        end
    end

    if (yEndPixRem < TOLERANCE) then
        if delY >= 0 then
            yEndPix = yEndPix - yEndPixRem
        else
            yEndPix = yEndPix + yEndPixRem
        end
    elseif (yEndPixRem > (SIZE - TOLERANCE)) then
        if delY >= 0 then
            yEndPix = yEndPix + (SIZE - yEndPixRem)
        else
            yEndPix = yEndPix - (SIZE - yEndPixRem)
        end
    end
    
    local xEnd = (xEndPix - X_ORIGIN) / SIZE
    local yEnd = (yEndPix - Y_ORIGIN) / SIZE
    local xStart = (xStartPix - X_ORIGIN) / SIZE
    local yStart = (yStartPix - Y_ORIGIN) / SIZE

    local isValidEnd = false

    -- undo false moves and execute right ones
    if (xStart % 1) == 0 and (xEnd % 1) == 0 then
        if (yStart % 1) == 0 and (yEnd % 1) == 0 then
            if (xEndPixRem < TOLERANCE or xEndPixRem > (SIZE - TOLERANCE)) or (yEndPixRem < TOLERANCE or yEndPixRem > (SIZE - TOLERANCE)) then
                if matrix[xStart][yStart] ~= nil and matrix[xStart][yStart].ball ~= nil then
                    isValidEnd = ___execute_end_move(xStart, yStart, xEnd, yEnd)
                end
            end
        end
    end
        
    if isValidEnd == false then
        if matrix[xStart][yStart] ~= nil and matrix[xStart][yStart].ball ~= nil then
            transition.moveTo(matrix[xStart][yStart].ball, {
                time = 200,
                x = xStartPix,
                y = yStartPix
            });
        end
    end
    organize_balls("ended")
    
    return true
end

-- touch listener
function _touch_listener(event)
    if event.phase == "began" then
        event.target.markX = event.target.x
        event.target.markY = event.target.y

        -- organise any misplaced balls
        organize_balls(event.phase)

    elseif event.phase == "moved" and (event.target.markX ~= nil and event.target.markY ~= nil) then
        local moved_x = (event.x - event.xStart) + event.target.markX
        local moved_y = (event.y - event.yStart) + event.target.markY
        event.target.x = moved_x
        event.target.y = moved_y
        
    elseif event.phase == "ended" and (event.target.markX ~= nil  and event.target.markY ~= nil) then
        local ended_x = (event.x - event.xStart) + event.target.markX
        local ended_y = (event.y - event.yStart) + event.target.markY

        -- if markX or markY coords are setup correctly execute touch
        -- else organize balls with cancelled event
        local xOrdRem = (event.target.markX - X_ORIGIN) % SIZE
        local yOrdRem = (event.target.markY - Y_ORIGIN) % SIZE
        if xOrdRem == 0 and yOrdRem == 0 then
            __end_touch_handler(event.target.markX, event.target.markY, ended_x, ended_y)
        else
            organize_balls("cancelled")
        end
    
    elseif event.phase == "cancelled" then
        organize_balls(event.phase)
    end
end