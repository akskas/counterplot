demo = {}

local function onComplete( event )
    print( "video session ended" )
    native.showAlert( "Video Complete!", event.errorMessage, { "OK" } )
end

function demo.show()
    media.playVideo( "demo.mp4", true, onComplete )
    return true
end