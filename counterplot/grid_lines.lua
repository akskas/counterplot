grid_lines ={}

function grid_lines.show()
    if grid.is_hidden == false then
        return true
    end

    -- re-initialize grid
    grid = nil
    grid = {
        x = {},
        y = {}, 
        left_diag = nil,
        right_diag = nil, 
        triangles = {},
        circles = {},
        appName = {}
    }

    grid.app_name = display.newText("Player", 100, 200)
    grid.app_name:setFillColor(1, 1, 1)

    -- make squares
  
    -- horizontal lines
    for j = -2, 2 do
        grid.y[j] = display.newLine( X_ORIGIN - 2*SIZE, Y_ORIGIN + j*SIZE, X_ORIGIN + 2*SIZE, Y_ORIGIN + j*SIZE )
        grid.y[j].strokeWidth = LINE_WIDTH
        grid.y[j]:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )
    end

    -- vertical lines
    for i = -2, 2 do
        if i == 0 then
            grid.x[i] = display.newLine( X_ORIGIN + i*SIZE, Y_ORIGIN - 3*SIZE, X_ORIGIN + i*SIZE, Y_ORIGIN + 3*SIZE )
            grid.x[i].strokeWidth = LINE_WIDTH
            grid.x[i]:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )
        else
            grid.x[i] = display.newLine( X_ORIGIN + i*SIZE, Y_ORIGIN - 2*SIZE, X_ORIGIN + i*SIZE, Y_ORIGIN + 2*SIZE )
            grid.x[i].strokeWidth = LINE_WIDTH
            grid.x[i]:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )
        end  
    end

    -- left diagonals
    grid.left_diag = display.newLine( X_ORIGIN + SIZE, Y_ORIGIN - 3*SIZE, X_ORIGIN - 2*SIZE, Y_ORIGIN )
    grid.left_diag:append( X_ORIGIN + SIZE, Y_ORIGIN + 3*SIZE )
    grid.left_diag.strokeWidth = LINE_WIDTH
    grid.left_diag:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )

    -- right diagonals
    grid.right_diag = display.newLine( X_ORIGIN - SIZE, Y_ORIGIN - 3*SIZE, X_ORIGIN + 2*SIZE, Y_ORIGIN )
    grid.right_diag:append( X_ORIGIN - SIZE, Y_ORIGIN + 3*SIZE )
    grid.right_diag.strokeWidth = LINE_WIDTH
    grid.right_diag:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )

    -- triangles
    for i = -3, 3, 6 do
        grid.triangles[i] = display.newLine( X_ORIGIN - SIZE, Y_ORIGIN + i*SIZE, X_ORIGIN + SIZE, Y_ORIGIN + i*SIZE )
        grid.triangles[i].strokeWidth = LINE_WIDTH
        grid.triangles[i]:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )
    end

    -- create cicles
    for i = -2, 2 do
        grid.circles[i] = {}
        for j = -3, 3 do
            local modI = modulus(i)
            local modJ = modulus(j)
            if modJ <= 2 then
                grid.circles[i][j] = display.newCircle(0, 0, RADIUS)
                grid.circles[i][j].strokeWidth = LINE_WIDTH
                grid.circles[i][j]:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )
                grid.circles[i][j].x = X_ORIGIN + i*SIZE 
                grid.circles[i][j].y = Y_ORIGIN + j*SIZE
            else
                if modJ == 3 and (modI == 1 or modI == 0) then
                    grid.circles[i][j] = display.newCircle(0, 0, RADIUS)
                    grid.circles[i][j].strokeWidth = LINE_WIDTH
                    grid.circles[i][j]:setStrokeColor( COLOR_LINE_R, COLOR_LINE_G, COLOR_LINE_B )
                    grid.circles[i][j].x = X_ORIGIN + i*SIZE 
                    grid.circles[i][j].y = Y_ORIGIN + j*SIZE
                end
            end
        end
    end

    grid.is_hidden = false

end

function grid_lines.hide()

    if grid.is_hidden == true then
        return true
    end

    if grid.app_name ~= nil then
        grid.app_name:removeSelf()
        grid.app_name = nil
    end

    -- hide squares
  
    -- horizontal lines
    for j = -2, 2 do
        if grid.y[j] ~= nil then
            grid.y[j]:removeSelf()
            grid.y[j] = nil
        end
    end

    -- vertical lines
    for i = -2, 2 do
        if grid.x[i] ~= nil then
            grid.x[i]:removeSelf()
            grid.x[i] = nil
        end  
    end

    -- left diagonals
    if grid.left_diag ~= nil then
        grid.left_diag:removeSelf()
        grid.left_diag = nil
    end

    -- right diagonals
    if grid.right_diag ~= nil then
        grid.right_diag:removeSelf()
        grid.right_diag = nil
    end 

    -- triangles
    for i = -3, 3, 6 do
        if grid.triangles[i] ~= nil then
            grid.triangles[i]:removeSelf()
            grid.triangles[i] = nil
        end
    end

    -- create cicles
    for i = -2, 2 do
        for j = -3, 3 do
            local modI = modulus(i)
            local modJ = modulus(j)
            if modJ <= 2 then
                if grid.circles[i][j] ~= nil then
                    grid.circles[i][j]:removeSelf()
                    grid.circles[i][j] = nil
                end
            else
                if modJ == 3 and (modI == 1 or modI == 0) then
                    if grid.circles[i][j] ~= nil then
                        grid.circles[i][j]:removeSelf()
                        grid.circles[i][j] = nil
                    end
                end
            end
        end
        grid.circles[i] = nil
    end

    grid.is_hidden = true

end