play_button = {}

local function handle_play()
    rules.hide()
    play_button.hide() 
    demo_button.hide()

    rules_button.show()
    grid_lines.show()
    balls.show()
end

function play_button.show()
    if play_button_display.is_hidden == false then
        return true
    end

    play_button_display.button = widget.newButton({
        left = WIDTH-(WIDTH/3),
        top = HEIGHT-(0.4*HEIGHT),
        id = "play_button",
        label = "PLAY",
        fontSize = 45,
        width = 200,
        height = 100,
        shape = "roundedRect",
        cornerRadius = 15,
        fillColor = { default={0.75,0.87,11,1}, over={0.56,0.78,1,1} },
        onPress = handle_play
    })

    play_button_display.is_hidden = false
    return true
end

function play_button.hide()
    if play_button_display.is_hidden == true then
        return true
    end

    if play_button_display.button ~= nil then
        play_button_display.button:removeSelf()
    end

    play_button_display = {
        button = {},
        is_hidden = true
    }

end