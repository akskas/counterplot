winner ={}

function winner.show()
    if winner_screen.is_hidden == false then
        return true
    end

    winner_screen.ball  = display.newCircle(0, 0, RADIUS/1.5)
    winner_screen.ball.x = X_ORIGIN - 100
    winner_screen.ball.y = Y_ORIGIN - 200
    
    if WHOS_TURN == 'B' then
        winner_screen.ball:setFillColor( 0.25, 1, 0.25, 1 )
    else
        winner_screen.ball:setFillColor( 1, 0.25, 0.25, 1 )
    end

    transition.to(winner_screen.ball, {
        time = 500,
        iterations = 6000,
        xScale = 1.2,
        yScale = 1.2
    });

    winner_screen.text = display.newText("WON!", X_ORIGIN + 50, Y_ORIGIN - 200)
    winner_screen.text:setFillColor(1, 1, 1)

    winner_screen.is_hidden = false

    play_again_button.show()
    return true
end

function winner.hide()
    if winner_screen.is_hidden == true then
        return true
    end

    if winner_screen.ball ~= nil then
        winner_screen.ball:removeSelf()
    end
    
    if winner_screen.text ~= nil then
        winner_screen.text:removeSelf()
    end

    winner_screen = {
        ball = {},
        text = {},
        is_hidden = true
    }

    return true
end