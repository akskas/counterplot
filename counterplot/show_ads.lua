show_ads ={}

local bannerPlacement = "bottom-banner-320x50"
local interstitialPlacement = "interstitial-1"
local close_ads = {}

function show_ads.show()
	local bannerX = WIDTH-70
	local bannerY = HEIGHT-210
	local interstitialX = WIDTH-150
	local interstitialY = 200

	local adsPlacement = bannerPlacement
	local adsX = bannerX
	local adsY = bannerY

	if ADS_TYPE == "interstitial" then
		adsPlacement = interstitialPlacement
		adsX = interstitialX
		adsY = interstitialY
	end

	function close_button()
		coronaAds.hide(adsPlacement)
		if close_ads ~= nil then
			close_ads:removeSelf()
		end
	end

	function show_button()
		close_ads = widget.newButton({
			left = adsX,
        	top = adsY,
        	id = "close_button",
        	label = "X",
        	fontSize = 50,
        	width = 50,
    		height = 50,
        	fillColor = { default={1,1,1,1}},
        	onPress = close_button
		})
	end

	coronaAds.hide()
	coronaAds.show(adsPlacement, true)
	if ADS_TYPE ~= 'interstitial' then
		show_button()
	end

end