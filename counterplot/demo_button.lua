demo_button = {}

local function handle_demo()
    demo.show()
end

function demo_button.show()
    if demo_button_display.is_hidden == false then
        return true
    end

    demo_button_display.button = widget.newButton({
        left = WIDTH-(0.85*WIDTH),
        top = HEIGHT - (0.4*HEIGHT),
        id = "demo_button",
        label = "WATCH DEMO",
        fontSize = 45,
        width = 400,
        height = 100,
        shape = "roundedRect",
        cornerRadius = 15,
        fillColor = { default={0.75,0.87,11,1}, over={0.56,0.78,1,1} },
        onPress = handle_demo
    })

    demo_button_display.is_hidden = false
    return true
end

function demo_button.hide()
    if demo_button_display.is_hidden == true then
        return true
    end

    if demo_button_display.button ~= nil then
        demo_button_display.button:removeSelf()
    end

    demo_button_display = {
        button = {},
        is_hidden = true
    }
end