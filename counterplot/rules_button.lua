rules_button = {}

local function handle_rules()
    rules_button.hide()
    grid_lines.hide()
    balls.hide()

    rules.show()
end

function rules_button.show()
    if rules_button_display.is_hidden == false then
        return true
    end

    rules_button_display.button = widget.newButton({
        x = WIDTH/2,
        y = 200,
        id = "rules_button",
        label = "RULES",
        fontSize = 45,
        width = 200,
        height = 100,
        shape = "roundedRect",
        cornerRadius = 15,
        fillColor = { default={0.75,0.87,11,1}, over={0.56,0.78,1,1} },
        onPress = handle_rules
    })

    rules_button_display.is_hidden = false
    return true
end

function rules_button.hide()
    if rules_button_display.is_hidden == true then
        return true
    end

    if rules_button_display.button ~= nil then
        rules_button_display.button:removeSelf()
    end

    rules_button_display = {
        button = {},
        is_hidden = true
    }

end

return rules_button