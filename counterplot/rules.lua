rules = {}

function rules.show()

    if rules_screen.is_hidden == false then
        return true
    end

    local rules_y = 300
    local rules_diff_y = 100
    local line_diff_y = 80

    rules_screen.rule_text = display.newText('RULES', 0, rules_y)
    rules_screen.rule_text:setFillColor(1, 1, 1)
    rules_screen.rule_text.x = display.contentCenterX

    rules_screen.rule_1 = display.newText('1. A ball can be moved to an empty circle.', 500, rules_y + rules_diff_y)
    rules_screen.rule_1:setFillColor(1, 1, 1)

    rules_screen.rule_2.first = display.newText('2. You can acquire other player\'s ball by', 480, rules_y + 2*rules_diff_y)
    rules_screen.rule_2.first:setFillColor(1, 1, 1)
   
    rules_screen.rule_2.second = display.newText('passing over it to an empty circle', 460, rules_y + 2*rules_diff_y + line_diff_y)
    rules_screen.rule_2.second:setFillColor(1, 1, 1)


    rules_screen.rule_3.first = display.newText('3. The one who acquires all the balls of', 470, rules_y + 3*rules_diff_y + line_diff_y)
    rules_screen.rule_3.first:setFillColor(1, 1, 1)

    rules_screen.rule_3.second = display.newText('other player wins.', 290, rules_y + 3*rules_diff_y + 2*line_diff_y)
    rules_screen.rule_3.second:setFillColor(1, 1, 1)

    rules_screen.is_hidden = false

    rules_button.hide()
    
    demo_button.show()
    play_button.show()

    return true
end

function rules.hide()

    if rules_screen.is_hidden == true then
        return true
    end

    if rules_screen.rule_text ~= nil then
        rules_screen.rule_text:removeSelf()
    end

    if rules_screen.rule_1 ~= nil then
        rules_screen.rule_1:removeSelf()
    end

    if rules_screen.rule_2.first ~= nil then
        rules_screen.rule_2.first:removeSelf()
    end

    if rules_screen.rule_2.second ~= nil then
        rules_screen.rule_2.second:removeSelf()
    end

    if rules_screen.rule_3.first ~= nil then
        rules_screen.rule_3.first:removeSelf()
    end

    if rules_screen.rule_3.second ~= nil then
        rules_screen.rule_3.second:removeSelf()
    end

    rules_screen = {
        rule_text = {},
        rule_1 = {},
        rule_2 = {},
        rule_3 = {},
        is_hidden = true
    }

    return true
end

return rules