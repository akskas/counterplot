balls = {}

local function _get_new_ball (x, y, player)
    local ball  = display.newCircle(0, 0, RADIUS)
    ball.x = X_ORIGIN + x*SIZE; ball.y = Y_ORIGIN + y*SIZE
    if player == 'A' then
        ball:setFillColor( 0.25, 1, 0.25, 1 )
    else
        ball:setFillColor( 1, 0.25, 0.25, 1 )
    end
    
    ball:addEventListener("touch", _touch_listener)
    return ball
end

local function _show_current_player (player)
    local ball  = display.newCircle(0, 0, RADIUS/2)
    ball.x = 220  
    ball.y = 200
    if player == 'A' then
        ball:setFillColor( 0.25, 1, 0.25, 1 )
    else
        ball:setFillColor( 1, 0.25, 0.25, 1 )
    end

    transition.to(ball, {
        time = 500,
        iterations = 10000,
        xScale = 1.5,
        yScale = 1.5
    });

    matrix.current_player = ball
    return true
end

-- main function
function balls.show()

    if matrix.is_hidden == false then
        return true
    end

    WHOS_TURN = 'A'
    LAST_MOVE_DIFF = 0
    LAST_X_START = nil
    LAST_Y_START = nil
    LAST_X_END = nil
    LAST_Y_END = nil
    LAST_PLAYER = nil
    IS_UNDONE = false

    -- initialize matrix
    for x = -2, 2 do
        matrix[x] = {}
        for y = -3, 3 do
            local modX = modulus(x)
            local modY = modulus(y)
            if modY <= 2 then
                if (y > 0) or (y == 0 and x < 0) then
                    matrix[x][y] = {x = x, y = y, player = 'A'}
                elseif (y < 0) or (y == 0 and x > 0) then
                    matrix[x][y] = {x = x, y = y, player = 'B'} 
                else
                    matrix[x][y] = {x = x, y = y, player = 'nil'}
                end
            else
                if modY == 3 and (modX == 1 or modX == 0) then
                    if y > 0 then
                        matrix[x][y] = {x = x, y = y, player = 'A'}
                    else
                        matrix[x][y] = {x = x, y = y, player = 'B'}
                    end
                end
            end
        end
    end

    -- add balls to matrix
    for x = -2, 2 do
        for y = -3, 3 do
            if (matrix[x][y] ~= nil) and (x ~= 0 or y ~= 0) then
                matrix[x][y].ball = _get_new_ball(x, y, matrix[x][y].player)
                if matrix[x][y].player ~= WHOS_TURN then
                    matrix[x][y].ball:removeEventListener("touch", _touch_listener)
                end
            end
        end
    end
    _show_current_player(WHOS_TURN)
    matrix.is_hidden = false
end

function balls.hide()
    if matrix.is_hidden == true then
        return true
    end

    for x = -2, 2 do
        for y = -3, 3 do
            local modX = modulus(x)
            local modY = modulus(y)
            if modY <= 2 then
                if matrix[x][y].ball ~= nil then
                    matrix[x][y].ball:removeSelf()
                end
                matrix[x][y] = nil
            else
                if modY == 3 and (modX == 1 or modX == 0) then
                    if matrix[x][y].ball ~= nil then
                        matrix[x][y].ball:removeSelf()
                    end
                    matrix[x][y] = nil
                end
            end
        end
    end
    matrix.current_player:removeSelf()
    matrix.is_hidden = true
end

function balls.show_undo()
    if IS_UNDONE == true then
        return true
    end

    undo_button.hide()

    -- handle middle point
    if LAST_MOVE_DIFF == 2 then
        local midX = (LAST_X_START + LAST_X_END) / 2
        local midY = (LAST_Y_START + LAST_Y_END) / 2

        local MIDDLE_PLAYER = nil
        if LAST_PLAYER == 'A' then
            MIDDLE_PLAYER = 'B'
        elseif LAST_PLAYER == 'B' then
            MIDDLE_PLAYER = 'A'
        end

        matrix[midX][midY].player = MIDDLE_PLAYER
        matrix[midX][midY].ball = _get_new_ball(midX, midY, MIDDLE_PLAYER)
        matrix[midX][midY].ball:removeEventListener("touch", _touch_listener) 
    end

    -- handle starting point
    matrix[LAST_X_START][LAST_Y_START].player = LAST_PLAYER
    matrix[LAST_X_START][LAST_Y_START].ball = _get_new_ball(LAST_X_START, LAST_Y_START, LAST_PLAYER)
    matrix[LAST_X_START][LAST_Y_START].ball:removeEventListener("touch", _touch_listener)
    
    -- handle ending point
    matrix[LAST_X_END][LAST_Y_END].player = 'nil'
    matrix[LAST_X_END][LAST_Y_END].ball:removeEventListener("touch", _touch_listener)
    matrix[LAST_X_END][LAST_Y_END].ball:removeSelf()
    matrix[LAST_X_END][LAST_Y_END].ball = nil

    change_current_player(LAST_PLAYER)

    -- change WHOS_TURN
    WHOS_TURN = 'A'
    if LAST_PLAYER == 'B' then
        WHOS_TURN = 'B'
    end

    -- reset touch listeners
    for x = -2, 2 do
        for y = -3, 3 do
            if matrix[x][y] ~= nil and matrix[x][y].player ~= 'nil' and matrix[x][y].ball ~= nil then
                if matrix[x][y].player ~= WHOS_TURN then
                    matrix[x][y].ball:removeEventListener("touch", _touch_listener)
                else
                    matrix[x][y].ball:addEventListener("touch", _touch_listener)
                end
            end
        end
    end

    IS_UNDONE = true
    LAST_MOVE_DIFF = 0
    LAST_X_START = nil
    LAST_Y_START = nil
    LAST_X_END = nil
    LAST_Y_END = nil
    LAST_PLAYER = nil

    organize_balls("ended")
end