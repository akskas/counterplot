-- global variables
widget = require('widget')
inspect = require('inspect')

-- dimensions
WIDTH = 1080
HEIGHT = 1920
SIZE = 220
RADIUS = 60
X_ORIGIN = 540
Y_ORIGIN = 1100
LINE_WIDTH = 8
TOLERANCE = 70 -- lesser the tolerance the better

-- colors
COLOR_LINE_R = 0.6
COLOR_LINE_G = 0.6
COLOR_LINE_B = 0.6

-- game variables
WHOS_TURN = 'A'
ADS_TYPE = "interstitial"
undo_button_display = {
	button = {},
	is_hidden = true
}
demo_button_display = {
	button = {},
	is_hidden = true
}
rules_button_display = {
	button = {},
	is_hidden = true
}
play_button_display = {
	button = {},
	is_hidden = true
}
play_again_button_display = {
  button = {},
  is_hidden = true
}
rules_screen = {
    rule_text = {},
    rule_1 = {},
    rule_2 = {},
    rule_3 = {},
    is_hidden = true
}
winner_screen = {
    ball = {},
    text = {},
    is_hidden = true
}
current_player = {}
matrix = {
	current_player = {},
	is_hidden = true
}
grid = {
	x = {},
	y = {}, 
	left_diag = nil,
	right_diag = nil, 
	triangles = {},
	circles = {},
	app_name = {},
	is_hidden = true
}

-- undo variable
LAST_MOVE_DIFF = 0
LAST_X_START = nil
LAST_Y_START = nil
LAST_X_END = nil
LAST_Y_END = nil
LAST_PLAYER = nil
IS_UNDONE = false

-- start app
local header = require('header')
header.show()

rules_button.show()
grid_lines.show()
balls.show()

local function adListener(event)
	if (event.phase == "init") then
		-- show interstitial ads
    	ADS_TYPE = "interstitial"
    	show_ads.show()
	end
end

-- initilize coronaAds
coronaAds = require( "plugin.coronaAds" )
coronaAds.init( "0f1beeb2-d323-40f1-9caf-c6c6d5f9d8dc", adListener )