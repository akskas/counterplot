local coronaAds = require( "plugin.coronaAds" )

local bannerPlacement = "top-banner-320x50"
local interstitialPlacement = "interstitial-1"

local function adListener( event )

	if ( event.phase == "init" ) then
		coronaAds.show( bannerPlacement, false )
	end
end

coronaAds.init( "0f1beeb2-d323-40f1-9caf-c6c6d5f9d8dc", adListener )